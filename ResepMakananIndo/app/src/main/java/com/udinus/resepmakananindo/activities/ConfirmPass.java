package com.udinus.resepmakananindo.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.udinus.resepmakananindo.R;

public class ConfirmPass extends AppCompatActivity {

    EditText edit_Text_7,edit_text_8,edit_text_9;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_pass);

        edit_Text_7 = findViewById(R.id.edit_text_7);
        edit_text_8 = findViewById(R.id.edit_text_8);
        edit_text_9 = findViewById(R.id.edit_text_9);

        edit_Text_7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View V) {
                edit_Text_7.setBackground(getResources().getDrawable(R.drawable.bg_edit_text_blue_border));
            }
        });
        edit_text_8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View V) {
                edit_text_8.setBackground(getResources().getDrawable(R.drawable.bg_edit_text_blue_border));
            }
        });
        edit_text_9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View V) {
                edit_text_9.setBackground(getResources().getDrawable(R.drawable.bg_edit_text_blue_border));
            }
        });
    }
    public void clickLeft (View view) {
        Intent i = new Intent(ConfirmPass.this, LoginPages.class);
        startActivity(i);
    }
    public void clickConfirm(View view) {
        Intent i = new Intent(ConfirmPass.this,LoginPages.class);
        startActivity(i);
    }

}