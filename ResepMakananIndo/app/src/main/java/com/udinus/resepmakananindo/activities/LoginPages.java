package com.udinus.resepmakananindo.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.udinus.resepmakananindo.R;

public class LoginPages extends AppCompatActivity {
    TextView text_view;
    EditText edit_text_1,edit_text_2;
    // Deklarasi variabel editTextEmail dengan tipe EditText
    EditText editTextEmail;
    // Deklarasi variabel editTextPassword dengan tipe EditText
    EditText editTextPassword;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_pages);

        text_view = findViewById(R.id.text_view);
        edit_text_1 = findViewById(R.id.edit_text_1);
        edit_text_2 = findViewById(R.id.edit_text_2);
        // Binding edt_txt_email ke variabel editTextEmail
        editTextEmail = findViewById(R.id.edit_text_1);
        // Binding edt_txt_password ke variabel editTextPassword
        editTextPassword = findViewById(R.id.edit_text_2);

        edit_text_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edit_text_1.setBackground(getResources().getDrawable(R.drawable.bg_edit_text_blue_border));
            }
        });


        edit_text_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edit_text_1.setBackground(getResources().getDrawable(R.drawable.bg_edit_text_blue_border));
            }
        });
        text_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LoginPages.this, SignUp.class);
                startActivity(i);
            }
        });

    }

    public void clickLogin(View view) {
        // Validasi input email dan password kosong
        if (TextUtils.isEmpty(editTextEmail.getText().toString().trim())
                &&
                TextUtils.isEmpty(editTextPassword.getText().toString().trim())) {
            Toast.makeText(view.getContext(), "Email dan Password Tidak Boleh Kosong!", Toast.LENGTH_LONG).show();
        }
        // Validasi input email kosong
        if (TextUtils.isEmpty(editTextEmail.getText().toString().trim())) {
            Toast.makeText(view.getContext(), "Email Tidak Boleh Kosong!", Toast.LENGTH_LONG).show();
        }
        // Validasi inputan tipe email
        else if (!isValidEmail(editTextEmail.getText().toString().trim())) {
            Toast.makeText(view.getContext(), "Email Tidak Valid", Toast.LENGTH_LONG).show();
        }
        // Validasi password kosong
        else if (TextUtils.isEmpty(editTextPassword.getText().toString().trim())) {
            Toast.makeText(view.getContext(), "Password Tidak Boleh Kosong!", Toast.LENGTH_LONG).show();
        } else {
            Intent i = new Intent(LoginPages.this, Homepage_main.class);
            startActivity(i);
        }
    }

    public void clickForgot(View view) {
        Intent i = new Intent(LoginPages.this, Forgotpass.class);
        startActivity(i);
    }
    public static boolean isValidEmail (CharSequence email){
        return (Patterns.EMAIL_ADDRESS.matcher(email).matches());
    }

}