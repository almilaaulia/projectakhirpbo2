package com.udinus.resepmakananindo.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.udinus.resepmakananindo.R;

public class SignUp extends AppCompatActivity {
    TextView text_view,text_view_3;
    EditText edit_text_1,edit_text_2,edit_text_3,edit_text_4,edit_text_5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        text_view = findViewById(R.id.text_view);
        text_view_3 = findViewById(R.id.text_view_3);
        edit_text_1 = findViewById(R.id.edit_text_1);
        edit_text_2 = findViewById(R.id.edit_text_2);
        edit_text_3 = findViewById(R.id.edit_text_3);
        edit_text_4 = findViewById(R.id.edit_text_4);
        edit_text_5 = findViewById(R.id.edit_text_5);

        text_view.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View V){
                startActivity(new Intent(SignUp.this,LoginPages.class));

            }
        });

        edit_text_1.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View V){
                edit_text_1.setBackground(getResources().getDrawable(R.drawable.bg_edit_text_blue_border));

            }
        });
        edit_text_2.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View V){
                edit_text_2.setBackground(getResources().getDrawable(R.drawable.bg_edit_text_blue_border));

            }
        });
        edit_text_3.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View V){
                edit_text_3.setBackground(getResources().getDrawable(R.drawable.bg_edit_text_blue_border));

            }
        });
        edit_text_4.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View V){
                edit_text_4.setBackground(getResources().getDrawable(R.drawable.bg_edit_text_blue_border));

            }
        });
        edit_text_5.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View V){
                edit_text_5.setBackground(getResources().getDrawable(R.drawable.bg_edit_text_blue_border));

            }
        });
    }

    public void clickLeft(View view) {
        Intent i = new Intent(SignUp.this,LoginPages.class);
        startActivity(i);
    }

    public void clickCreateAcc(View view) {
        Intent i = new Intent(SignUp.this,Homepage_main.class);
        startActivity(i);
    }
}
